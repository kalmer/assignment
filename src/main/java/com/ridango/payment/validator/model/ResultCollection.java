package com.ridango.payment.validator.model;

import com.ridango.payment.model.result.Message;

import java.util.ArrayList;
import java.util.List;

public class ResultCollection {

    private final List<Message> messages;

    private ResultCollection() {
        this.messages = new ArrayList<>();
    }

    public ResultCollection addIf(boolean condition, Message message) {
        if (condition) {
            messages.add(message);
        }
        return this;
    }

    public List<Message> asCollection() {
        return messages;
    }

    public static ResultCollection messages() {
        return new ResultCollection();
    }
}
