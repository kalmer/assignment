package com.ridango.payment.validator.model;

import com.ridango.payment.model.result.Message;

import java.util.List;

public interface ValidationRule<T> {

    List<Message> apply(T type);

}
