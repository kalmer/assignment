package com.ridango.payment.validator.model;

import com.ridango.payment.model.result.Message;

import java.util.ArrayList;
import java.util.List;

import static com.ridango.payment.util.CollectionUtils.isNotEmpty;

public class ValidationResult {

    private final List<Message> messages;

    public ValidationResult() {
        this.messages = new ArrayList<>();
    }

    public boolean hasErrors() {
        return !messages.isEmpty();
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void add(Message message) {
        if (message != null && !message.getValue().isEmpty()) {
            this.messages.add(message);
        }
    }

    public void add(List<Message> messages) {
        if (isNotEmpty(messages)) {
            messages.forEach(this::add);
        }
    }
}
