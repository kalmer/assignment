package com.ridango.payment.validator.rules;

import com.ridango.payment.model.Payment;
import com.ridango.payment.model.result.Message;
import com.ridango.payment.validator.model.ValidationRule;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.ridango.payment.model.result.Messages.*;
import static com.ridango.payment.validator.model.ResultCollection.messages;

@Component
public class PaymentRequiredDataRule implements ValidationRule<Payment> {

    @Override
    public List<Message> apply(Payment payment) {
        return messages()
                .addIf(payment.getAmount() == null, AMOUNT_REQUIRED)
                .addIf(payment.getReceiverAccountId() == null, RECEIVER_ACCOUNT_REQUIRED)
                .addIf(payment.getSenderAccountId() == null, SENDER_ACCOUNT_REQUIRED)
                .asCollection();
    }
}
