package com.ridango.payment.validator.rules;

import com.ridango.payment.model.Account;
import com.ridango.payment.model.Payment;
import com.ridango.payment.model.result.Message;
import com.ridango.payment.repository.AccountRepository;
import com.ridango.payment.validator.model.ValidationRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.ridango.payment.model.result.Messages.NON_EXISTING_RECEIVER_ACCOUNT;
import static com.ridango.payment.model.result.Messages.NON_EXISTING_SENDER_ACCOUNT;

@Component
public class ExistingAccountsRule implements ValidationRule<Payment> {

    private final AccountRepository accountRepository;

    @Autowired
    public ExistingAccountsRule(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public List<Message> apply(Payment payment) {
        List<Message> messages = new ArrayList<>();
        if (payment.getReceiverAccountId() != null && payment.getSenderAccountId() != null) {
            Account senderAccount = accountRepository.get(payment.getSenderAccountId());
            if (senderAccount == null) {
                messages.add(NON_EXISTING_SENDER_ACCOUNT);
            }

            Account receiverAccount = accountRepository.get(payment.getReceiverAccountId());
            if (receiverAccount == null) {
                messages.add(NON_EXISTING_RECEIVER_ACCOUNT);
            }
        }
        return messages;
    }
}
