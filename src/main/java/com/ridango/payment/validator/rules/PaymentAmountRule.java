package com.ridango.payment.validator.rules;

import com.ridango.payment.model.Account;
import com.ridango.payment.model.Payment;
import com.ridango.payment.model.result.Message;
import com.ridango.payment.repository.AccountRepository;
import com.ridango.payment.validator.model.ValidationRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static com.ridango.payment.model.result.Messages.*;
import static com.ridango.payment.util.MathUtils.is;
import static com.ridango.payment.util.PrecisionUtils.isNotInNumericPrecision;

@Component
public class PaymentAmountRule implements ValidationRule<Payment> {

    private final AccountRepository accountRepository;

    @Autowired
    public PaymentAmountRule(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public List<Message> apply(Payment payment) {
        if (payment.getSenderAccountId() != null && payment.getReceiverAccountId() != null) {
            if (is(payment.getAmount()).lessThanZero()) {
                return Collections.singletonList(NEGATIVE_AMOUNT);
            }

            if (isNotInNumericPrecision(payment.getAmount())) {
                return Collections.singletonList(INVALID_AMOUNT);
            }

            Account senderAccount = accountRepository.get(payment.getSenderAccountId());
            if (senderAccount != null) {
                BigDecimal accountBalance = senderAccount.getBalance();
                if (is(accountBalance).lessThan(payment.getAmount())) {
                    return Collections.singletonList(SENDER_ACCOUNT_MISSING_RESOURCES);
                }
            }
        }
        return null;
    }
}
