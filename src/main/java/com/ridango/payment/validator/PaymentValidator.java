package com.ridango.payment.validator;

import com.ridango.payment.model.Payment;
import com.ridango.payment.validator.model.ValidationResult;
import com.ridango.payment.validator.model.ValidationRule;
import com.ridango.payment.validator.rules.ExistingAccountsRule;
import com.ridango.payment.validator.rules.PaymentAmountRule;
import com.ridango.payment.validator.rules.PaymentRequiredDataRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class PaymentValidator implements Validator<Payment> {

    private final List<ValidationRule<Payment>> rules;

    @Autowired
    public PaymentValidator(PaymentRequiredDataRule paymentRequiredDataRule,
                            ExistingAccountsRule existingAccountsRule,
                            PaymentAmountRule paymentAmountRule) {
        this.rules = Arrays.asList(paymentRequiredDataRule, existingAccountsRule, paymentAmountRule);
    }

    public ValidationResult validate(Payment payment) {
        return apply(payment, rules);
    }
}
