package com.ridango.payment.validator;

import com.ridango.payment.model.result.Message;
import com.ridango.payment.validator.model.ValidationResult;
import com.ridango.payment.validator.model.ValidationRule;

import java.util.List;

public interface Validator<T> {

    ValidationResult validate(T type);

    default ValidationResult apply(T type, List<ValidationRule<T>> rules) {
        ValidationResult validationResult = new ValidationResult();
        for (ValidationRule<T> rule : rules) {
            List<Message> messages = rule.apply(type);
            validationResult.add(messages);
        }
        return validationResult;
    }
}
