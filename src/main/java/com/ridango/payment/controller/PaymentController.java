package com.ridango.payment.controller;

import com.ridango.payment.model.Payment;
import com.ridango.payment.model.result.Result;
import com.ridango.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {

    private final PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping("/payment")
    public Result<Payment> create(@RequestBody Payment payment) {
        return paymentService.create(payment);
    }
}
