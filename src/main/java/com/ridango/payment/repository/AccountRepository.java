package com.ridango.payment.repository;

import com.ridango.payment.model.Account;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface AccountRepository {

    Account get(@Param("accountId") Long accountId);

    int changeBalance(@Param("accountId") Long accountId,
                      @Param("balance") BigDecimal balance);

}
