package com.ridango.payment.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LockingRepository {

    Long lockAccount(@Param("accountId") Long accountId);

}
