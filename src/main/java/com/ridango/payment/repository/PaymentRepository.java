package com.ridango.payment.repository;

import com.ridango.payment.model.Payment;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository {

    void save(@Param("payment") Payment payment);

}
