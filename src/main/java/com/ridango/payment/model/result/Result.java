package com.ridango.payment.model.result;

import com.ridango.payment.validator.model.ValidationResult;

import java.util.List;

import static com.ridango.payment.util.CollectionUtils.isNotEmpty;
import static java.util.Collections.singletonList;

public class Result<T> {

    private T data;
    private List<Message> errors;

    public Result() {}

    public Result(T data) {
        this.data = data;
    }

    public Result(ValidationResult validationResult) {
        if (validationResult != null && validationResult.hasErrors()) {
            errors = validationResult.getMessages();
        }
    }

    public Result(List<Message> messages) {
        if (isNotEmpty(messages)) {
            this.errors = messages;
        }
    }

    public Result(Message message) {
        if (message != null) {
            errors = singletonList(message);
        }
    }

    public T getData() {
        return data;
    }

    public List<Message> getErrors() {
        return errors;
    }

    public boolean failed() {
        return isNotEmpty(errors);
    }

    public boolean isSuccessful() {
        return !failed();
    }
}
