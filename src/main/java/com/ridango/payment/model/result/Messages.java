package com.ridango.payment.model.result;

public class Messages {

    public static final Message NON_EXISTING_SENDER_ACCOUNT = of("error.payment.nonExistingSenderAccount");
    public static final Message NON_EXISTING_RECEIVER_ACCOUNT = of("error.payment.nonExistingReceiverAccount");
    public static final Message NEGATIVE_AMOUNT = of("error.payment.amountCannotBeNegative");
    public static final Message INVALID_AMOUNT = of("error.payment.invalidAmount");
    public static final Message SENDER_ACCOUNT_MISSING_RESOURCES = of("error.payment.senderAccountMissingResources");

    public static final Message AMOUNT_REQUIRED = of("error.payment.amountCannotBeNull");
    public static final Message RECEIVER_ACCOUNT_REQUIRED = of("error.payment.receiverAccountMissing");
    public static final Message SENDER_ACCOUNT_REQUIRED = of("error.payment.senderAccountMissing");

    public static final Message FAILED_TO_CHANGE_PAYER_ACCOUNT_BALANCE = of("error.account.payerAccountChangeFailed");
    public static final Message FAILED_TO_CHANGE_RECEIVER_ACCOUNT_BALANCE = of("error.account.receiverAccountChangeFailed");

    public static final Message UNABLE_TO_OBTAIN_LOCK = of("error.lock.unableToObtain");

    private static Message of(String value) {
        return new Message(value);
    }
}
