package com.ridango.payment.model.result;

import com.ridango.payment.validator.model.ValidationResult;

public class ResultFactory {

    public static <T> Result<T> success() {
        return new Result<>();
    }

    public static <T> Result<T> success(T data) {
        return new Result<>(data);
    }

    public static <T> Result<T> error(ValidationResult validationResult) {
        return new Result<>(validationResult);
    }

    public static <T> Result<T> error(Result<Message> result) {
        return new Result<>(result.getErrors());
    }

    public static <T> Result<T> error(Message message) {
        return new Result<>(message);
    }
}
