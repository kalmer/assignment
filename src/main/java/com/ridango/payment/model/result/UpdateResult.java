package com.ridango.payment.model.result;

public class UpdateResult {

    private final int affectedRows;

    public UpdateResult(int affectedRows) {
        this.affectedRows = affectedRows;
    }

    public boolean failed() {
        return affectedRows < 0;
    }
}
