package com.ridango.payment.service;

import com.ridango.payment.model.Account;
import com.ridango.payment.model.Payment;
import com.ridango.payment.model.result.Result;
import com.ridango.payment.model.result.UpdateResult;
import com.ridango.payment.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import static com.ridango.payment.model.result.ResultFactory.error;
import static com.ridango.payment.model.result.ResultFactory.success;
import static com.ridango.payment.model.result.Messages.FAILED_TO_CHANGE_PAYER_ACCOUNT_BALANCE;
import static com.ridango.payment.model.result.Messages.FAILED_TO_CHANGE_RECEIVER_ACCOUNT_BALANCE;

@Service
public class AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Result<Payment> pay(Payment payment) {
        UpdateResult senderAccountBalanceChange = changeSenderAccountBalance(payment);
        if (senderAccountBalanceChange.failed()) {
            return error(FAILED_TO_CHANGE_PAYER_ACCOUNT_BALANCE);
        }

        UpdateResult receiverAccountBalanceChange = changeReceiverAccountBalance(payment);
        if (receiverAccountBalanceChange.failed()) {
            return error(FAILED_TO_CHANGE_RECEIVER_ACCOUNT_BALANCE);
        }

        return success(payment);
    }

    private UpdateResult changeSenderAccountBalance(Payment payment) {
        Account account = accountRepository.get(payment.getSenderAccountId());
        BigDecimal balance = account.getBalance().subtract(payment.getAmount());
        return new UpdateResult(accountRepository.changeBalance(payment.getSenderAccountId(), balance));
    }

    private UpdateResult changeReceiverAccountBalance(Payment payment) {
        Account account = accountRepository.get(payment.getReceiverAccountId());
        BigDecimal balance = account.getBalance().add(payment.getAmount());
        return new UpdateResult(accountRepository.changeBalance(payment.getReceiverAccountId(), balance));
    }
}
