package com.ridango.payment.service;

import com.ridango.payment.model.Payment;
import com.ridango.payment.model.result.Message;
import com.ridango.payment.model.result.Result;
import com.ridango.payment.repository.LockingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.function.Supplier;

import static com.ridango.payment.model.result.Messages.UNABLE_TO_OBTAIN_LOCK;
import static com.ridango.payment.model.result.ResultFactory.error;
import static com.ridango.payment.model.result.ResultFactory.success;

@Service
public class LockingService {

    private final LockingRepository lockingRepository;

    @Autowired
    public LockingService(LockingRepository lockingRepository) {
        this.lockingRepository = lockingRepository;
    }

    public Result<Message> lockAccounts(Payment payment) {
        Result<Message> senderAccountLock = lock(() -> lockingRepository.lockAccount(payment.getSenderAccountId()));
        if (senderAccountLock.failed()) {
            return senderAccountLock;
        }
        return lock(() -> lockingRepository.lockAccount(payment.getReceiverAccountId()));
    }

    private Result<Message> lock(Supplier<Long> lockingMethod) {
        try {
            Long lock = lockingMethod.get();
            if (lock == null) {
                return error(UNABLE_TO_OBTAIN_LOCK);
            }
            return success();
        } catch (Exception e) {
            return error(UNABLE_TO_OBTAIN_LOCK);
        }
    }
}
