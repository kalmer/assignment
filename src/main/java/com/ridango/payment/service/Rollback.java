package com.ridango.payment.service;

import org.springframework.transaction.interceptor.TransactionInterceptor;

public class Rollback {

    public static void rollback() {
        TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
    }

    public static <T> T rollback(T returnThis) {
        rollback();
        return returnThis;
    }
}
