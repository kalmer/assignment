package com.ridango.payment.service;

import com.ridango.payment.model.Payment;
import com.ridango.payment.model.result.Message;
import com.ridango.payment.model.result.Result;
import com.ridango.payment.repository.PaymentRepository;
import com.ridango.payment.validator.PaymentValidator;
import com.ridango.payment.validator.model.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.ridango.payment.model.result.ResultFactory.error;
import static com.ridango.payment.model.result.ResultFactory.success;
import static com.ridango.payment.service.Rollback.rollback;

@Service
public class PaymentService {

    private final PaymentValidator paymentValidator;
    private final PaymentRepository paymentRepository;
    private final AccountService accountService;
    private final LockingService lockingService;

    @Autowired
    public PaymentService(PaymentValidator paymentValidator,
                          PaymentRepository paymentRepository,
                          AccountService accountService,
                          LockingService lockingService) {
        this.paymentValidator = paymentValidator;
        this.paymentRepository = paymentRepository;
        this.accountService = accountService;
        this.lockingService = lockingService;
    }

    @Transactional
    public Result<Payment> create(Payment payment) {
        Result<Message> lockAccounts = lockingService.lockAccounts(payment);
        if (lockAccounts.failed()) {
            return error(lockAccounts);
        }

        ValidationResult validation = paymentValidator.validate(payment);
        if (validation.hasErrors()) {
            return error(validation);
        }

        Result<Payment> accountBalanceChange = accountService.pay(payment);
        if (accountBalanceChange.failed()) {
            return rollback(accountBalanceChange);
        }

        paymentRepository.save(payment);
        return success(payment);
    }
}
