package com.ridango.payment.util;

import java.util.List;

public class CollectionUtils {

    private CollectionUtils() {}

    public static boolean isEmpty(List<?> list) {
        return list == null || list.isEmpty();
    }

    public static boolean isNotEmpty(List<?> list) {
        return !isEmpty(list);
    }
}
