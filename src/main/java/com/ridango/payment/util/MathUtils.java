package com.ridango.payment.util;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;

public class MathUtils {

    private final BigDecimal value;

    private MathUtils(BigDecimal value) {
        this.value = value;
    }

    public static MathUtils is(BigDecimal value) {
        return new MathUtils(value);
    }

    public boolean lessThan(BigDecimal other) {
        return value != null && other != null && value.compareTo(other) < 0;
    }

    public boolean lessThanZero() {
        return lessThan(ZERO);
    }
}
