package com.ridango.payment.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PrecisionUtils {

    private static final int MAX_ALLOWED_FRACTIONAL_PART_LENGTH = 2;
    private static final long MAX_ALLOWED_INTEGRAL_PART = 9999999999L;

    public static boolean isNotInNumericPrecision(BigDecimal value) {
        if (value != null) {
            int fractionalPartLength = Math.max(0, value.stripTrailingZeros().scale());
            if (fractionalPartLength > MAX_ALLOWED_FRACTIONAL_PART_LENGTH) {
                return true;
            }

            long integralPart = value.setScale(0, RoundingMode.DOWN).longValue();
            return integralPart > MAX_ALLOWED_INTEGRAL_PART;
        }
        return false;
    }
}
