DROP SEQUENCE IF EXISTS payment_id_seq;
DROP SEQUENCE IF EXISTS account_id_seq;
DROP TABLE IF EXISTS payment CASCADE;
DROP TABLE IF EXISTS account CASCADE;

CREATE SEQUENCE account_id_seq;
CREATE TABLE account(
  id BIGINT PRIMARY KEY,
  name VARCHAR(50) NOT NULL,
  balance NUMERIC(18, 2) NOT NULL
);

CREATE SEQUENCE payment_id_seq;
CREATE TABLE payment(
  id BIGINT PRIMARY KEY,
  sender_account_id BIGINT NOT NULL,
  receiver_account_id BIGINT NOT NULL,
  amount NUMERIC(10, 2) NOT NULL,
  timestamp TIMESTAMP NOT NULL,
  FOREIGN KEY (sender_account_id) REFERENCES account(id),
  FOREIGN KEY (receiver_account_id) REFERENCES account(id)
);
